import { Weather } from './classes.js';

// ELEMENTS

const weathers = [];

for (let i = 0; i < 5; i++) {
  let day = document.getElementById(`day${i}`);
  let img = document.getElementById(`img${i}`);
  let temp = document.getElementById(`temp${i}`);
  let minTemp = document.getElementById(`min${i}`);
  let maxTemp = document.getElementById(`max${i}`);
  weathers.push(new Weather(day, img, temp, minTemp, maxTemp));
}

const elements = {
  country: document.getElementById('country'),
  city: document.getElementById('city'),
  timezone: document.getElementById('timezone'),
  weathers,
};

export default elements;
