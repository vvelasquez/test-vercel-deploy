import elements from './elements.js';

export default function setWeather(data) {
  //    Setting city
  elements.city.innerHTML = data.title;
  elements.country.innerHTML = data.parent.title;
  elements.timezone.innerHTML = data.timezone;
  //   Setting weathers
  for (let i = 0; i < 5; i++) {
    elements.weathers[i].day.innerHTML =
      data.consolidated_weather[i].applicable_date;
    elements.weathers[
      i
    ].img.src = `https://www.metaweather.com/static/img/weather/${data.consolidated_weather[i].weather_state_abbr}.svg`;
    elements.weathers[i].temp.innerHTML =
      Math.round(data.consolidated_weather[i].the_temp) + '°';
    elements.weathers[i].minTemp.innerHTML =
      Math.round(data.consolidated_weather[i].min_temp) + '°';
    elements.weathers[i].maxTemp.innerHTML =
      Math.round(data.consolidated_weather[i].max_temp) + '°';
  }
}
