import setWeather from './setWeather.js';

async function fetchWeather(id) {
  if (!id) {
    let data = await fetch(
      `https://api.allorigins.win/get?url=${encodeURIComponent(
        'https://www.metaweather.com/api/location/2487956/'
      )}`
    );
    data = await data.json();
    return setWeather(JSON.parse(data.contents));
  } else {
    console.log(id);
    let data = await fetch(
      `https://api.allorigins.win/get?url=${encodeURIComponent(
        `https://www.metaweather.com/api/location/${id}/`
      )}`
    );
    data = await data.json();
    return setWeather(JSON.parse(data.contents));
  }
}

fetchWeather();

document.getElementById('search').addEventListener('keyup', (event) => {
  let div = document.getElementById('test');
  for (let i = 0; i < div.childNodes.length; i++) {
    div.removeChild(div.lastChild);
  }

  fetch(
    `https://api.allorigins.win/get?url=${encodeURIComponent(
      `https://www.metaweather.com/api/location/search/?query=${event.target.value}`
    )}`
  )
    .then((r) => r.json())
    .then((r) => {
      r = JSON.parse(r.contents);
      for (let i = 0; i < 5; i++) {
        let button = document.createElement('button');
        button.innerHTML = r[i].title;
        button.id = r[i].woeid;
        button.onclick = (event) => fetchWeather(event.target.id);
        div.appendChild(button);
      }
    });
});
